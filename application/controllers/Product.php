<?php
class Product extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->load->model('product_model');
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');


	}
	function index(){
		$this->load->view('product_view');
	}

	public function _example_output($output = null)
	{
		$this->load->view('example.php',(array)$output);

	}
	public function test(){
	try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('product');
			//$crud->set_subject('Office');
			$crud->required_fields('product_code');
			$crud->columns('product_code','product_name','product_price');

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function product()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$crud = new grocery_CRUD();

		$crud->set_theme('twitter-bootstrap');
		$output = $this->grocery_crud->render();
		//$this->set_theme('twitter-bootstrap');

		$this->_example_output($output);
	}

	function product_data(){
		$data=$this->product_model->product_list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->product_model->save_product();
		echo json_encode($data);
	}

	function update(){
		$data=$this->product_model->update_product();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->product_model->delete_product();
		echo json_encode($data);
	}

}